
// #4
package mockito;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SomeBusinessImplTest {

    //You cant autowire an component/bean during a unit test because the spring application context is
    //not available. Well, you can.. but id be pretty slow to run the application context for every unittest.

    @Autowired
    SomeBusinessImpl someBusiness;

    @Test
    public void testFindGreatest() {
        someBusiness.findTheGreatestFromAllData();
    }

}
