// #1
package junit;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class AssertTest {

    @Test
    public void test() {
        boolean condition = true;

        //Assert condition is equal to..
        assertEquals(true, condition);

        //Assert condition true
        assertTrue(condition);

        //Assert condition is false
//        assertFalse(condition);
    }

}
