// #2
package junit;

import org.junit.*;

import static org.junit.Assert.assertEquals;

public class SumCalculatorTest {

    SumCalculator sumCalculator = new SumCalculator();

    @Test
    public void sum_with3numbers() {
        System.out.println("test1");
        assertEquals(6, sumCalculator.calculateSum(new int[] {1, 2, 3}));
    }

    @Test
    public void sum_with1numbers() {
        System.out.println("test2");
        assertEquals(3, sumCalculator.calculateSum(new int[] { 3 }));
    }

    @Before
    public void before() {
        System.out.println("before");
    }

    @After
    public void after() {
        System.out.println("After");
    }

    @BeforeClass
    public static void beforeClass() {
        System.out.println("Before class");
    }

    @AfterClass
    public static void afterClass() {
        System.out.println("After class");
    }

}
