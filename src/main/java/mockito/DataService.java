package mockito;

public interface DataService {
    int[] retrieveAllData();
}
